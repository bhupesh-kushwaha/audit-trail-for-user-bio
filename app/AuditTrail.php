<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditTrail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'description'
    ];

    /**
     * The uposition that related to the Bio Details.
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
