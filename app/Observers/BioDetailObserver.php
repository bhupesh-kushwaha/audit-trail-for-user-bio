<?php

namespace App\Observers;

use App\AuditTrail;
use App\BioDetail;
use Illuminate\Support\Facades\Auth;

class BioDetailObserver
{
    /**
     * Handle the bio detail "updated" event.
     *
     * @param  \App\BioDetail  $bioDetail
     * @return void
     */
    public function updating(BioDetail $bioDetail)
    {
        $updateKeys = array();

        if( $bioDetail->getDirty()['bio_description'] ) {
            $updateKeys = [
                'new' => $bioDetail->getDirty()['bio_description'],
                'old' => $bioDetail->getOriginal()['bio_description'],
            ];
        }

        if( empty($updateKeys) ){
            $updateKeys = [
                'old' => $bioDetail->getOriginal()['bio_description'] ,
                'new' => ""
            ];
        }

        AuditTrail::create([
            'user_id' => Auth::user()->id,
            'description' => serialize($updateKeys)
        ]);
    }
}
