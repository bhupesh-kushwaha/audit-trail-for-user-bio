<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BioDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'headline', 'position_id', 'profile_pic', 'bio_description'
    ];

    /**
     * The uposition that related to the Bio Details.
     */
    public function position()
    {
        return $this->hasOne(Position::class, 'position_id', 'id');
    }
}
