<?php

namespace App\Http\Traits;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Relation;
use App\Skill;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

trait HelperTrait
{
    /**
     * BioDeatils profile upload and remove old profile image from path.
     *
     * @param oblect $request
     * @param oblect $bioDetails
     * @return string
     */
    public function bioDetailsFileUpload($request, $bioDetails)
    {
        $profileImageName = $request->profile_pic_old;

        if ($request->hasFile('profile_pic')) {
            $image = $request->file('profile_pic');
            $profileImageName = rand(). time() . '.' . $image->getClientOriginalName();
            $image->move( public_path() . "/profile_pic/", $profileImageName);

            $image_path = public_path()."/profile_pic/".$bioDetails->profile_pic;

            if(File::exists($image_path)) {
                File::delete($image_path);
            }
        }

        return $profileImageName;
    }

    /**
     * Get diff string between old to new string
     *
     * @param string $old
     * @param string $new
     * @return string
     */
    public function getContainsDiff($old, $new){
        $originalOld = $old;
        $originalNew = $new;

        $old = strtolower($old);
        $new = strtolower($new);

        $from_start = strspn($old ^ $new, "\0");
        $from_end = strspn(strrev($old) ^ strrev($new), "\0");

        $old_end = strlen($old) - $from_end;
        $new_end = strlen($new) - $from_end;

        $start = substr($new, 0, $from_start);
        $end = substr($new, $new_end);

        $new_diff = substr($originalNew, $from_start, $new_end - $from_start);
        $old_diff = substr($originalOld, $from_start, $old_end - $from_start);

        $new = "- $start<del style='background-color:#ffcccc'>$old_diff</del>$end<br/>";
        $new .= "+ $start<ins style='background-color:#ccffcc'>$new_diff</ins>$end";

        return $new;
    }
}
