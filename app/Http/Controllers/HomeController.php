<?php

namespace App\Http\Controllers;

use App\AuditTrail;
use App\BioDetail;
use App\Http\Requests\PostionRequest;
use App\Http\Traits\HelperTrait;
use App\Position;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use HelperTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $bioDetails = BioDetail::orderBy('id', 'desc')->first();

        $position = Position::orderBy('name', 'asc')->pluck('name', 'id');

        $defaultProfile = url('/'). '/profile_pic/user-purple.png';

        $auditTrail = AuditTrail::orderBy('created_at', 'desc')->get()->map( function($item) {
            $newItem['user_name'] = $item->users->name;

            $newItem['created'] = $item->created_at->format('d M, Y H:i:s');

            $value = unserialize($item->description);

            $newItem['description'] = $this->getContainsDiff($value['old'] , $value['new']);;

            return $newItem;
        })
        ->all();


        $auditViewBox = [];

        foreach($auditTrail as $item) {
            $auditViewBox[] = view('Components.Box', compact('item'))->render();
        }

        return view('home', compact('position', 'bioDetails', 'defaultProfile', 'auditViewBox'));
    }

    /**
     * save Bio Detaisl
     *
     * @param PostionRequest $request
     * @return json
     */
    public function saveBioDetails(Request $request)
    {
       // dd( $request->all() );

        $bioDetails = BioDetail::where('id',$request->profile_id)->first();

        $profileImageName = $this->bioDetailsFileUpload($request, $bioDetails);

        $bioDetails->first_name = $request->first_name;
        $bioDetails->last_name = $request->last_name;
        $bioDetails->headline = $request->headline;
        $bioDetails->position_id = $request->position_id;
        $bioDetails->profile_pic = $profileImageName;
        $bioDetails->profile_pic = $profileImageName;
        $bioDetails->bio_description = $request->bio_description;
        $bioDetails->save();

        return redirect()->route('home')->with('status', 'User successfully updated');
    }

    /**
     * save new postion
     *
     * @param PostionRequest $request
     * @return json
     */
    public function savePostion(PostionRequest $request)
    {
        $checkSkill = Position::where('name', $request->name)->first();

        if( !$checkSkill ) {
            $skill = new Position();
            $skill->name = $request->name;
            $skill->save();
        }

        return response()->json([ 'message' => 'New position added' ]);
    }

}
