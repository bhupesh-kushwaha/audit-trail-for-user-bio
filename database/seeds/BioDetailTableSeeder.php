<?php

use App\BioDetail;
use App\Position;
use Illuminate\Database\Seeder;

class BioDetailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $position = Position::create([
            'name' => 'Internship'
        ]);

        BioDetail::create([
            'first_name' => 'Demo',
            'last_name' => 'Tester',
            'headline' => 'Do it',
            'position_id' => $position->id,
            'profile_pic' => '',
        ]);
    }
}
