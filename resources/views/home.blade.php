@extends('layouts.app')

@push('style')
    <link href="{{ asset('css/bio-detail.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="container-fluid main-secction">
    <div class="row">
        <form
            id="bs-validate-demo"
            method="post"
            action="{{ route('bio-detail.save') }}"
            class="{{ ($errors->any()) ? 'was-validated': ''}}"
            enctype="multipart/form-data"
            novalidate
        >
            @csrf

            <div class="col-md-12 col-sm-12 col-xs-12 image-section">
                <img  src="https://via.placeholder.com/980x100/C/O%20https://placeholder.com/000000/FFFFFF/?text=Bhupesh+Kushwaha" />
            </div>

            <div class="row user-left-part">
                <div class="col-md-4 col-sm-4 col-xs-12 user-profil-part pull-left cmb-5">
                    <div class="row ">

                        <div class="col-md-12 col-md-12-sm-12 col-xs-12 user-image text-center border-2">
                            <label for="profile_pic">
                                <img
                                    id="preview-image"
                                    src="{{ isset($bioDetails->profile_pic) && $bioDetails->profile_pic ? url('/') . "/profile_pic/" . $bioDetails->profile_pic : $defaultProfile }}"
                                    class="rounded-circle"
                                />
                                <span style="vertical-align: bottom; cursor: pointer"  class="text-primary"><i class="fa fa-edit"></i> edit</span>
                            </label>
                        </div>

                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 pull-right profile-right-section mb-5"></div>

                <div class="col-md-12 col-sm-12 col-xs-12 pull-right profile-right-section">
                    <div class="row profile-right-section-row">
                        <div class="col-md-12 profile-header">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 profile-header-section1 pull-left">

                                    <div class="row justify-content-center">
                                        <div class="col-md-10">
                                            @if(session()->has('status'))
                                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <strong>Success!</strong> {{ session('status') }}

                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif

                                            @if($errors->any())
                                                <div class="alert alert-danger">
                                                    <p><strong>Opps Something went wrong</strong></p>
                                                    <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            @if(session('success'))
                                                <div class="alert alert-success">{{session('success')}}</div>
                                            @endif

                                            <input
                                                type="hidden"
                                                id="profile_id"
                                                name="profile_id"
                                                value="{{ isset($bioDetails->id) && $bioDetails->id ? $bioDetails->id : '' }}"
                                            />

                                            <input type="file" id="profile_pic" name="profile_pic" class="d-none" />

                                            <input
                                                type="hidden"
                                                id="profile_pic_old"
                                                name="profile_pic_old"
                                                value="{{ isset( $bioDetails->profile_pic ) &&  $bioDetails->profile_pic  ? $bioDetails->profile_pic : '' }}"
                                            />

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="first_name">Fiest Name:</label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            id="first_name"
                                                            name="first_name"
                                                            value="{{ isset($bioDetails->first_name ) && $bioDetails->first_name  ? $bioDetails->first_name : '' }}"
                                                            placeholder="Enter First Name"
                                                        />
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="last_name">Last Name:</label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            id="last_name"
                                                            name="last_name"
                                                            value="{{ isset($bioDetails->last_name) && $bioDetails->last_name ? $bioDetails->last_name : '' }}"
                                                            placeholder="Enter Last Name"
                                                        />
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="headline">Headlines:</label>
                                                        <textarea
                                                            class="form-control"
                                                            id="headline"
                                                            name="headline"
                                                            placeholder="Enter Your Headline"
                                                        >{{ isset($bioDetails->headline) && $bioDetails->headline ? $bioDetails->headline : '' }}</textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="position_id">Current Position</label>
                                                        <select class="form-control" id="position_id" name="position_id">
                                                            <option value="">-- Select Position --</option>
                                                            @foreach ($position as $key =>$value)
                                                                <option
                                                                    value="{{ $key }}"
                                                                    {{ isset($bioDetails->position_id) && $bioDetails->position_id == $key ? 'selected' : '' }}
                                                                >
                                                                    {{$value}}
                                                                </option>
                                                            @endforeach
                                                        </select>

                                                        <a href="javascript:void(0)" class="text-info mt-3 mb-3 float-right" id="create-new-position">Add new position</a>
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="bio_description">Description</label>
                                                        <textarea
                                                            class="form-control"
                                                            id="bio_description"
                                                            name="bio_description"
                                                            placeholder="Description"
                                                        >{{ isset($bioDetails->bio_description) && $bioDetails->bio_description ? $bioDetails->bio_description : '' }}</textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <h4> Audit Trail Lists </h4>
                                                    <div class="auto-scroll">
                                                        @forelse ($auditViewBox as $item)
                                                            {!! $item !!}
                                                        @empty
                                                            No records found!
                                                        @endforelse
                                                    </div>
                                                </div>

                                                <div class="col-md-12 text-center mt-5">
                                                    <button type="submit" class="btn btn-dark">Save Bio Details</button>
                                                </div>

                                            </div>

                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr/>

        </form>
    </div>
</div>

@include('Components.PositionModal')

@endsection

@push('script')
<script>

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview-image').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

    $(document).ready(function () {
        $("body").on('change', '#profile_pic', function(){
            readURL(this);
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#create-new-position').click(function () {
            $('#btn-save').val("create-position");
            $('#positionForm').trigger("reset");
            $('#positionCrudModal').html("All positions");
            $('#ajax-crud-modal').modal('show');
        });

        $('body').on('click', '#btn-save', function(e) {
            e.preventDefault();

            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            $.ajax({
                data: $('#positionForm').serialize(),
                url: "{{ route('position.save') }}",
                type: "post",
                dataType: 'json',
                success: function (data) {
                    location.reload();
                },
                error: function (reject) {
                    if( reject.status === 422 ) {
                        var errors = $.parseJSON(reject.responseText);
                        $.each(errors.errors, function (key, val) {
                            $("#" + key + "_error").text(val[0]);
                        });
                    }

                    $('#btn-save').html('Save');
                }
            });
        });
    });
</script>
@endpush
