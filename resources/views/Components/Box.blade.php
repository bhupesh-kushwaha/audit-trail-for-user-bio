@php

@endphp
<ul class="timeline">
    <li>
        <a href="javascript:void(0)">{{ $item['created'] }}</a>
        <a href="javascript:void(0)" class="float-right">{{ $item['user_name'] }}</a>
        <p>
            <span class="color-green">
                {!! $item['description'] !!}
            </span>
            <br/>
        </p>
    </li>
</ul>
