<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="positionCrudModal"></h4>
            </div>
            <div class="modal-body">
                <form id="positionForm" name="positionForm" class="form-horizontal">

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control modal-attributes" id="title" name="name" autocomplete="off">
                            <span class="text-danger" id="name_error"></span>
                        </div>
                    </div>

                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary"
                            id="btn-save" value="create">Save
                        </button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
